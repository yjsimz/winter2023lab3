public class Application
{
	public static void main (String[] args)
	{
		Student Sam = new Student(); 
		Sam.attendance = 10;
		Sam.name = "Sam";
		Sam.rScore = 31.2;
		
		Student Chris = new Student(); 
		Chris.attendance = 5;
		Chris.name = "Chris";
		Chris.rScore = 23.7;
		
		System.out.println (Sam.name + " " + Sam.attendance + " " + Sam.rScore);
		
		System.out.println (Sam.sorryImLate());
		System.out.println (Sam.raiseHand());
		
		System.out.println (Chris.sorryImLate());
		System.out.println (Chris.raiseHand());
		
		Student[] section3 = new Student[3];
		
		section3[0] = Sam;
		section3[1] = Chris;
		
		System.out.println(section3[0].attendance);
		//System.out.println(section3[2].attendance);
		
		section3[2] = new Student();
		//section3[2].Student = Ethan;
		section3[2].name = "Ethan";
		System.out.println (section3[2].name);
		
		System.out.println(section3[2].amountLearnt);
		}
}